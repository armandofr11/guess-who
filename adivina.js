//vme = valor minimo de eliminacion

var personajes = [
    {
        nombre: "akin",
        sexo: true,
        cabello: false,
        ojos_color: false,
        color_piel: false,
        sombrero: true,
        lentes: false
    },
    {
        nombre: "angie",
        sexo: false,
        cabello: true,
        ojos_color: false,
        color_piel: false,
        sombrero: true,
        lentes: false
    },
    {
        nombre: "bob",
        sexo: true,
        cabello: true,
        ojos_color: false,
        color_piel: false,
        sombrero: false,
        lentes: false
    },
    {
        nombre: "david",
        sexo: true,
        cabello: false,
        ojos_color: false,
        color_piel: true,
        sombrero: true,
        lentes: false
    },
    {
        nombre: "jason",
        sexo: "h",
        cabello: true,
        ojos_color: true,
        color_piel: false,
        sombrero: true,
        lentes: false
    },
    {
        nombre: "grace",
        sexo: false,
        cabello: true,
        ojos_color: false,
        color_piel: true,
        sombrero: false,
        lentes: false
    },
    {
        nombre: "megan",
        sexo: false,
        cabello: true,
        ojos_color: true,
        color_piel: false,
        sombrero: true,
        lentes: false
    },
    {
        nombre: "nick",
        sexo: true,
        cabello: false,
        ojos_color: true,
        color_piel: false,
        sombrero: true,
        lentes: true
    },
    {
        nombre: "rachel",
        sexo: false,
        cabello: true,
        ojos_color: false,
        color_piel: true,
        sombrero: true,
        lentes: false
    },
    {
        nombre: "ted",
        sexo: true,
        cabello: true,
        ojos_color: false,
        color_piel: false,
        sombrero: true,
        lentes: false
    },
    {
        nombre: "twitch",
        sexo: true,
        cabello: false,
        ojos_color: false,
        color_piel: false,
        sombrero: false,
        lentes: false
    },
    {
        nombre: "tyson",
        sexo: true,
        cabello: false,
        ojos_color: false,
        color_piel: true,
        sombrero: false,
        lentes: false
    }


]

var preguntas = [
    {
        id: "sexo",
        texto: "¿Tu personaje es hombre?",
        respuesta: Boolean,
        vme: 0

    },
    {
        id: "cabello",
        texto: "¿Tu personaje tiene cabello?",
        respuesta: Boolean,
        vme: 0

    },
    {
        id: "ojos",
        texto: "¿Tu personaje tiene ojos azules?",
        respuesta: Boolean,
        vme: 0

    },
    {
        id: "piel",
        texto: "¿Tu personaje tiene piel de color?",
        respuesta: Boolean,
        vme: 0

    },
    {
        id: "lentes",
        texto: "¿Tu personaje usa lentes?",
        respuesta: Boolean,
        vme: 0

    },
    {
        id: "accesorio",
        texto: "¿Tu personaje usa accesorio en la cabeza?",
        respuesta: Boolean,
        vme: 0

    }
]

function generarPregunta(piratas, preguntass) {
    var preguntaS;
    for (var i = 0; i < preguntass.length; i++) {
        var aciertos = 0;
        if (preguntas[i].id == "sexo") {
            for (var j = 0; j < piratas.length; j++) {
                if (piratas[j].sexo) {
                    aciertos++
                }
            }
            var auxiliar = piratas.length - aciertos;
            var min = (auxiliar < aciertos) ? auxiliar : aciertos;
            preguntas[i].vme = min;
        } else if (preguntas[i].id == "cabello") {
            for (var j = 0; j < piratas.length; j++) {
                if (piratas[j].cabello) {
                    aciertos++
                }
            }
            var auxiliar = piratas.length - aciertos;
            var min = (auxiliar < aciertos) ? auxiliar : aciertos;
            preguntas[i].vme = min;
        } else if (preguntas[i].id == "ojos") {
            for (var j = 0; j < piratas.length; j++) {
                if (piratas[j].ojos_color) {
                    aciertos++
                }
            }
            var auxiliar = piratas.length - aciertos;
            var min = (auxiliar < aciertos) ? auxiliar : aciertos;
            preguntas[i].vme = min;
        } else if (preguntas[i].id == "piel") {
            for (var j = 0; j < piratas.length; j++) {
                if (piratas[j].color_piel) {
                    aciertos++
                }
            }
            var auxiliar = piratas.length - aciertos;
            var min = (auxiliar < aciertos) ? auxiliar : aciertos;
            preguntas[i].vme = min;
        } else if (preguntas[i].id == "lentes") {
            for (var j = 0; j < piratas.length; j++) {
                if (piratas[j].lentes) {
                    aciertos++
                }
            }
            var auxiliar = piratas.length - aciertos;
            var min = (auxiliar < aciertos) ? auxiliar : aciertos;
            preguntas[i].vme = min;
        } else if (preguntas[i].id == "accesorio") {
            for (var j = 0; j < piratas.length; j++) {
                if (piratas[j].sombrero) {
                    aciertos++
                }
            }
            var auxiliar = piratas.length - aciertos;
            var min = (auxiliar < aciertos) ? auxiliar : aciertos;
            preguntas[i].vme = min;
        }
    }
    preguntaS = preguntass[0];
    for (var i = 0; i < preguntass.length; i++) {
        if (preguntaS.vme < preguntass[i].vme) {
            preguntaS = preguntass[i];
        }
    }
    return preguntaS;
}

function jugar() {
    var pregunta_s;
    while (personajes.length > 1) {
        //console.log(preguntas);
        console.log(preguntas)
        console.log(personajes)
        console.log("--------")
        pregunta_s = generarPregunta(personajes, preguntas);
        var pre_aux = [];
        for (var i = 0;  i < preguntas.length; i++){
            if(pregunta_s.texto != preguntas[i].texto){
                pre_aux.push(preguntas[i]);
            }
        }
        preguntas = pre_aux;

        var a = prompt(pregunta_s.texto);
        var resp_usr = (a == "si") ? true: false;
        var nuevos_personajes = [];
        var personajes_eliminados = [];
        personajes.forEach(personaje => {
            if(pregunta_s.id == "sexo"){
                if(personaje.sexo == resp_usr){
                    nuevos_personajes.push(personaje);
                } else {
                    personajes_eliminados.push(personaje);
                }
            } else if(pregunta_s.id == "cabello"){
                if(personaje.cabello == resp_usr){
                    nuevos_personajes.push(personaje);
                } else {
                    personajes_eliminados.push(personaje);
                }
            } else if(pregunta_s.id == "ojos"){
                if(personaje.ojos_color == resp_usr){
                    nuevos_personajes.push(personaje);
                } else {
                    personajes_eliminados.push(personaje);
                }
            } else if(pregunta_s.id == "piel"){
                if(personaje.color_piel == resp_usr){
                    nuevos_personajes.push(personaje);
                } else {
                    personajes_eliminados.push(personaje);
                }
            } else if(pregunta_s.id == "lentes"){
                if(personaje.lentes == resp_usr){
                    nuevos_personajes.push(personaje);
                } else {
                    personajes_eliminados.push(personaje);
                }
            } else if(pregunta_s.id == "accesorio"){
                if(personaje.sombrero == resp_usr){
                    nuevos_personajes.push(personaje);
                } else {
                    personajes_eliminados.push(personaje);
                }
            }
        })
        console.log(personajes_eliminados);
        personajes_eliminados.forEach(pj => {
            document.getElementById(pj.nombre).hidden = true;
        })
        personajes = nuevos_personajes;
    }
    console.log(personajes[0]);


}
